﻿namespace Unittests.School.Command.School
{
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;

    //[TestFixture]
    //public class UpdataTest
    //{
    //    [Test]
    //    public void UpdateOne_Success()
    //    {
    //        Microservice.School.Command.School.Insert insert = new Microservice.School.Command.School.Insert();
    //        Microservice.School.Command.School.Update update = new Microservice.School.Command.School.Update();

    //        Microservice.School.Database.School school = new Microservice.School.Database.School();
    //        school.Id = Guid.NewGuid();
    //        school.Name = "Vestre Skole Test";
    //        school.Municipality = "Norddujurs Kommune";
    //        school.Grades = new List<string>() { "1.a", "1.b", "2.a", "2.b", "3.a", "3.b" };

    //        insert.Execute(school);

    //        school.Name = "Test Skole Vestre";
    //        school.Municipality = "Test Kommune";
    //        school.Grades = new List<string>() { "1.a", "1.b" };

    //        update.Execute(school);
    //    }

    //    [Test]
    //    public void UpdateOne_WhereIdDoesNotExist()
    //    {
    //        Microservice.School.Command.School.Update update = new Microservice.School.Command.School.Update();

    //        Microservice.School.Database.School school = new Microservice.School.Database.School();
    //        school.Id = Guid.NewGuid();
    //        school.Name = "Vestre Skole Test";
    //        school.Municipality = "Norddujurs Kommune";
    //        school.Grades = new List<string>() { "1.a", "1.b", "2.a", "2.b", "3.a", "3.b" };

    //        Assert.Throws<ITUtil.Common.Base.ElementDoesNotExistException>(() => update.Execute(school));
    //    }
    //    [Test]
    //    public void UpdateOne_WhereIdIsNull()
    //    {
    //        Microservice.School.Command.School.Update update = new Microservice.School.Command.School.Update();

    //        Microservice.School.Database.School school = new Microservice.School.Database.School();
    //        school.Name = "Vestre Skole Test";
    //        school.Municipality = "Norddujurs Kommune";
    //        school.Grades = new List<string>() { "1.a", "1.b", "2.a", "2.b", "3.a", "3.b" };

    //        Assert.Throws<ITUtil.Common.Base.NullOrDefaultException>(() => update.Execute(school));
    //    }
    //    [Test]
    //    public void UpdateOne_WhereNameIsNull()
    //    {
    //        Microservice.School.Command.School.Update update = new Microservice.School.Command.School.Update();

    //        Microservice.School.Database.School school = new Microservice.School.Database.School();
    //        school.Id = Guid.NewGuid();
    //        school.Municipality = "Norddujurs Kommune";
    //        school.Grades = new List<string>() { "1.a", "1.b", "2.a", "2.b", "3.a", "3.b" };

    //        Assert.Throws<ITUtil.Common.Base.NullOrDefaultException>(() => update.Execute(school));
    //    }
    //    [Test]
    //    public void UpdateOne_WhereMunicipalityIsNull()
    //    {
    //        Microservice.School.Command.School.Update update = new Microservice.School.Command.School.Update();

    //        Microservice.School.Database.School school = new Microservice.School.Database.School();
    //        school.Id = Guid.NewGuid();
    //        school.Name = "Vestre Skole Test";
    //        school.Grades = new List<string>() { "1.a", "1.b", "2.a", "2.b", "3.a", "3.b" };

    //        Assert.Throws<ITUtil.Common.Base.NullOrDefaultException>(() => update.Execute(school));
    //    }
    //    [Test]
    //    public void UpdateOne_WhereClassesIsNull()
    //    {
    //        Microservice.School.Command.School.Update update = new Microservice.School.Command.School.Update();

    //        Microservice.School.Database.School school = new Microservice.School.Database.School();
    //        school.Id = Guid.NewGuid();
    //        school.Name = "Vestre Skole Test";
    //        school.Municipality = "Norddujurs Kommune";

    //        Assert.Throws<ITUtil.Common.Base.NullOrDefaultException>(() => update.Execute(school));
    //    }
    //}
}
