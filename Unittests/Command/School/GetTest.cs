﻿

namespace Unittests.School.Command.School
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using NUnit.Framework;

    [TestFixture]
    public class GetTest
    {
        [Test]
        public void GetOne_Success()
        {
            //Microservice.School.Command.School.Get cmd = new Microservice.School.Command.School.Get();
            //Microservice.School.Command.School.Insert insert = new Microservice.School.Command.School.Insert();

            //Microservice.School.DTO.ById id = new Microservice.School.DTO.ById();
            //id.Id = Guid.NewGuid();

            //Microservice.School.Database.School school = new Microservice.School.Database.School();
            //school.Id = id.Id;
            //school.Name = "Vestre Skole Test";
            //school.Municipality = "Norddujurs Kommune";
            //school.Grades = new List<string>() { "1.a", "1.b", "2.a", "2.b", "3.a", "3.b" };

            //insert.Execute(school);

            //var res = cmd.Execute(id);
            //Assert.AreEqual(school.Id, res.Id);
            //Assert.AreEqual(school.Name, res.Name);
            //Assert.AreEqual(school.Municipality, res.Municipality);
            //for (int i = 0; i < school.Grades.Count; i++)
            //{
            //    Assert.AreEqual(school.Grades[i], res.Grades[i]);
            //}

        }

        [Test]
        public void GetOne_WhereIdIsNull()
        {
            Microservice.School.Command.School.Get cmd = new Microservice.School.Command.School.Get();
            Microservice.School.Command.School.Insert insert = new Microservice.School.Command.School.Insert();

            Microservice.School.DTO.ById id = new Microservice.School.DTO.ById();

            Assert.Throws<ITUtil.Common.Base.NullOrDefaultException>(() => cmd.Execute(id));
        }
        [Test]
        public void GetOne_WhereIdDoesNotExist()
        {
            Microservice.School.Command.School.Get cmd = new Microservice.School.Command.School.Get();
            Microservice.School.Command.School.Insert insert = new Microservice.School.Command.School.Insert();

            Microservice.School.DTO.ById id = new Microservice.School.DTO.ById();
            id.Id = Guid.NewGuid();

            Assert.Throws<ITUtil.Common.Base.ElementDoesNotExistException>(() => cmd.Execute(id));
        }
    }
}
