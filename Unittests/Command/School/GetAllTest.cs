﻿namespace Unittests.School.Command.School
{
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;

    [TestFixture]
    public class GetAllTest
    {

        [Test]
        public void InsertOne_Success()
        {
            Microservice.School.Command.School.GetAll getAll = new Microservice.School.Command.School.GetAll();
            Microservice.School.Command.School.Insert insert = new Microservice.School.Command.School.Insert();

            Microservice.School.DTO.ById id = new Microservice.School.DTO.ById();
            id.Id = Guid.NewGuid();

            Microservice.School.DTO.InsertSchool school = new Microservice.School.DTO.InsertSchool();
            school.Id = id.Id;
            school.Name = "Vestre Skole Test";
            school.Municipality = "Norddujurs Kommune";
            school.Grades = new List<Microservice.School.Database.Grade>() {
                new Microservice.School.Database.Grade() { Id = Guid.NewGuid(),Year = Microservice.School.GradeYear.Zero, Name = "A" },
                new Microservice.School.Database.Grade() { Id = Guid.NewGuid(),Year = Microservice.School.GradeYear.Zero, Name = "" },
                new Microservice.School.Database.Grade() { Id = Guid.NewGuid(),Year = Microservice.School.GradeYear.Teacher, Name = "" },
                new Microservice.School.Database.Grade() { Id = Guid.NewGuid(),Year = Microservice.School.GradeYear.Teacher, Name = "Group 1" },
                new Microservice.School.Database.Grade() { Id = Guid.NewGuid(),Year = Microservice.School.GradeYear.Special, Name = "MB" },
            };
            school.DaysOff = new List<Microservice.School.Database.DayOff>() { new Microservice.School.Database.DayOff() { Id = Guid.NewGuid().ToString(), StartDate = DateTime.UtcNow, EndDate = DateTime.UtcNow, Name = "DummyDate", Type = Microservice.School.DayOffType.SchoolHoliday
            } };

            var beforinsert = getAll.Execute();
            insert.Execute(school);
            var afterinsert = getAll.Execute();

            Assert.IsTrue(afterinsert.list.Count > beforinsert.list.Count);
        }
    }
}
