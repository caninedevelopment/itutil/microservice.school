﻿namespace Unittests.School.Command.School
{
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;

    [TestFixture]
    public class DeleteTest
    {

        [Test]
        public void DeleteOne_Success()
        {
            Microservice.School.Command.School.Insert insert = new Microservice.School.Command.School.Insert();

            Microservice.School.DTO.ById id = new Microservice.School.DTO.ById();
            id.Id = Guid.NewGuid();

            Microservice.School.DTO.InsertSchool school = new Microservice.School.DTO.InsertSchool();
            school.Id = id.Id;
            school.Name = "Vestre Skole Test";
            school.Municipality = "Norddujurs Kommune";
            school.Grades = new List<Microservice.School.Database.Grade>() {
                new Microservice.School.Database.Grade() { Id = Guid.NewGuid(),Year = Microservice.School.GradeYear.Zero, Name = "A" },
                new Microservice.School.Database.Grade() { Id = Guid.NewGuid(),Year = Microservice.School.GradeYear.Zero, Name = "B" },
                new Microservice.School.Database.Grade() { Id = Guid.NewGuid(),Year = Microservice.School.GradeYear.Zero, Name = "C" }
            };
            school.DaysOff = new List<Microservice.School.Database.DayOff>() { new Microservice.School.Database.DayOff() { Id = Guid.NewGuid().ToString(), StartDate = DateTime.UtcNow, EndDate = DateTime.UtcNow, Name = "DummyDate", Type = Microservice.School.DayOffType.SchoolHoliday
            } };
            insert.Execute(school);


            Microservice.School.Command.School.Delete delete = new Microservice.School.Command.School.Delete();
            delete.Execute(id);
        }

        [Test]
        public void DeleteOne_WhereIdIsNull()
        {
            Microservice.School.Command.School.Delete cmd = new Microservice.School.Command.School.Delete();

            Microservice.School.DTO.ById id = new Microservice.School.DTO.ById();

            Assert.Throws<ITUtil.Common.Base.NullOrDefaultException>(() => cmd.Execute(id));
        }

        [Test]
        public void DeleteOne_WhereElementDoesNotExist()
        {
            Microservice.School.Command.School.Delete cmd = new Microservice.School.Command.School.Delete();

            Microservice.School.DTO.ById id = new Microservice.School.DTO.ById();
            id.Id = Guid.NewGuid();

            Assert.Throws<ITUtil.Common.Base.ElementDoesNotExistException>(() => cmd.Execute(id));
        }
    }
}
