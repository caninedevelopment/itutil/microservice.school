﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microservice.School
{
    public enum DayOffType
    {
        SchoolHoliday = 0,
        PublicHoliday = 1,
        RestingDay = 2
    }
}
