﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Microservice.School
{
    class Program
    {
        static void Main(string[] args)
        {
            ITUtil.Common.Console.Program.StartRabbitMq(new System.Collections.Generic.List<ITUtil.Common.Command.INamespace>()
            {
              new Command.School.Namespace()
            });
        }
    }
}
