﻿namespace Microservice.School.DTO
{
	using System;
	using System.Collections.Generic;
	using System.Text;
    public class ByExternalId
    {
        public string ExternalId { get; set; }
    }
}
