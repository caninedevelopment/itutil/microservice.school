﻿using Microservice.School.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microservice.School.DTO
{
    public class GetSchool
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets Municipality.
        /// </summary>
        public string Municipality { get; set; }
        /// <summary>
        /// Gets or sets Grades.
        /// </summary>
        public List<Grade> Grades { get; set; }
        /// <summary>
        /// Gets or sets DaysOff.
        /// </summary>
        public List<DayOff> DaysOff { get; set; }

        public GetSchool()
        {

        }
        public GetSchool(Database.School school)
        {
            this.Id = school.Id;
            this.Name = school.Name;
            this.Municipality = school.Municipality;
            this.Grades = school.Grades;
            this.DaysOff = school.DaysOff;
        }
    }
}
