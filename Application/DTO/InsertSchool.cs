﻿
namespace Microservice.School.DTO
{
    using Microservice.School.Database;
    using System;
    using System.Collections.Generic;
    public class InsertSchool
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets Municipality.
        /// </summary>
        public string Municipality { get; set; }
        /// <summary>
        /// Gets or sets Grades.
        /// </summary>
        public List<Grade> Grades { get; set; }
        /// <summary>
        /// Gets or sets DaysOff.
        /// </summary>
        public List<DayOff> DaysOff { get; set; }
    }
}
