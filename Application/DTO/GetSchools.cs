﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microservice.School.DTO
{
    public class GetSchools
    {
        public List<GetSchool> list { get; set; }
    }
}
