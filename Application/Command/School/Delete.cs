﻿
namespace Microservice.School.Command.School
{
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using System;
    using System.Collections.Generic;
    using System.Text;
    public class Delete : DeleteCommand<DTO.ById>
    {
        public Delete() : base("Delete school")
        {
            this.Claims.Add(Namespace.IsAdmin);
        }

        public override void Execute(DTO.ById input)
        {
            if (input.Id == Guid.Empty)
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("Id was not provided", "Id");
            }
            Database.School school = Namespace.db.GetSchoolById(input.Id);
            if (school == null)
            {
                throw new ITUtil.Common.Base.ElementDoesNotExistException("School does not exist", input.Id.ToString());
            }
            Namespace.db.DeleteSchool(input.Id);
        }
    }
}
