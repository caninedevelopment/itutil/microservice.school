﻿
namespace Microservice.School.Command.School
{
    using System.Collections.Generic;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using Newtonsoft.Json.Schema;

    public class Namespace : BaseNamespace
    {
        internal static Claim IsAdmin = new Claim() { dataContext = Claim.DataContextEnum.organisationClaims, description = new LocalizedString[] { new LocalizedString("en", "User is administrator, i.e. can change user rights") }, key = "Monosoft.School.isAdmin" };      
        public static DataContext db = new DataContext();
        public Namespace() : base("MongoDb_School", new ProgramVersion("1.0.0.0"))
        {
            this.Commands.AddRange(new List<ICommandBase>()
            {
                new Insert(),
                new Get(),
                new GetAll(),
                new Update(),
                new Delete()
            });
        }

    }
}
