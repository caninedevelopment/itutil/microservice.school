﻿
namespace Microservice.School.Command.School
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    public class Update : UpdateCommand<DTO.UpdateSchool>
    {
        public Update() : base("Update school")
        {
            this.Claims.Add(Namespace.IsAdmin);
        }
        public override void Execute(DTO.UpdateSchool input)
        {
            if (input.Id == Guid.Empty)
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("Id was not provided", "Id");
            }
            if (string.IsNullOrEmpty(input.Name))
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("Name was not provided", "Name");
            }
            if (string.IsNullOrEmpty(input.Municipality))
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("Municipality was not provided", "Municipality");
            }
            if (input.Grades == null || input.Grades.Any() == false)
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("Grades were not provided", "Grades");
            }
            if (input.DaysOff == null || input.DaysOff.Any() == false)
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("DaysOff were not provided", "DaysOff");
            }
            Database.School school = Namespace.db.GetSchoolById(input.Id);
            if (school == null)
            {
                throw new ITUtil.Common.Base.ElementDoesNotExistException("School does not exist", input.Id.ToString());
            }
            school.Name = input.Name;
            school.Municipality = input.Municipality;
            school.Grades = input.Grades;
            school.DaysOff = input.DaysOff;
            Namespace.db.UpdateSchool(input.Id, school);
        }
    }
}
