﻿
namespace Microservice.School.Command.School
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Schema;

    public class Insert : InsertCommand<DTO.InsertSchool>
    {
        public Insert() : base("Insert school")
        {
            this.Claims.Add(Namespace.IsAdmin);
        }

        public override void Execute(DTO.InsertSchool input)
        {
            if (input.Id == Guid.Empty)
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("Id was not provided", "Id");
            }
            if (string.IsNullOrEmpty(input.Name))
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("Name was not provided", "Name");
            }
            if (string.IsNullOrEmpty(input.Municipality))
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("Municipality was not provided", "Municipality");
            }
            if (input.Grades == null || input.Grades.Any() == false)
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("Grades were not provided", "Grades");
            }
            if (input.DaysOff == null || input.DaysOff.Any() == false)
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("DaysOff were not provided", "DaysOff");
            }



            Database.School school = Namespace.db.GetSchoolById(input.Id);
            if (school != null)
            {
                throw new ITUtil.Common.Base.ElementAlreadyExistsException("School already exists", input.Id.ToString());
            }
            school = new Database.School()
            {
                Id = input.Id,
                Name = input.Name,
                Municipality = input.Municipality,
                Grades = input.Grades,
                DaysOff = input.DaysOff
            };
            Namespace.db.InsertSchool(school);
        }
    }
}
