﻿
namespace Microservice.School.Command.School
{
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    using System;
    using System.Collections.Generic;
    public class Get : GetCommand<DTO.ById, DTO.GetSchool>
    {
        public Get() : base("Get school by id")
        {

        }

        public override DTO.GetSchool Execute(DTO.ById input)
        {

            if (input.Id == Guid.Empty)
            {
                throw new ITUtil.Common.Base.NullOrDefaultException("Id was not provided", "Id");
            }
            Database.School school = Namespace.db.GetSchoolById(input.Id);
            if (school == null)
            {
                throw new ElementDoesNotExistException("School does not exist", input.Id.ToString());
            }
            else
            {
                return new DTO.GetSchool(school);
            }

        }
    }
}
