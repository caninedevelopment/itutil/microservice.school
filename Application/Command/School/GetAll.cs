﻿
namespace Microservice.School.Command.School
{
    using System.Collections.Generic;
    using System.Linq;
    using ITUtil.Common.Base;
    using ITUtil.Common.Command;
    public class GetAll : GetCommand<object, DTO.GetSchools>
    {
        public GetAll() : base("Get schools")
        {

        }

        public override DTO.GetSchools Execute(object input = null) // Todo Maybe write another overload with no arguments in the GetCommand?
        {
            DTO.GetSchools result = new DTO.GetSchools();
            result.list = Namespace.db.GetSchools().Select(x => new DTO.GetSchool(x)).ToList();
            return result;
        }
    }
}
