﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microservice.School
{
    public enum GradeYear
    {
        Zero = 100,
        One = 101,
        Two = 102,
        Three = 103,
        Four = 104,
        Five = 105,
        Six = 106,
        Seven = 107,
        Eight = 108,
        Nine = 109,
        Ten = 110,
        Teacher = 1000,
        Special = 1100
    }
}
