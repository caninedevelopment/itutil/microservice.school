﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Microservice.School.Database
{
    public class DayOff
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DayOffType Type { get; set; }
    }
}
