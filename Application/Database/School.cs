﻿namespace Microservice.School.Database
{
    using MongoDB.Bson.Serialization.Attributes;
    using System;
    using System.Collections.Generic;
    using System.Text;
    public class School
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        [BsonId] // converts the Property to _id. Auto generates Guid.
        public Guid Id { get; set; }
        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets Municipality.
        /// </summary>
        public string Municipality { get; set; }
        /// <summary>
        /// Gets or sets Grades.
        /// </summary>
        public List<Grade> Grades { get; set; }
        /// <summary>
        /// Gets or sets DaysOff.
        /// </summary>
        public List<DayOff> DaysOff{ get; set; }
    }
}
