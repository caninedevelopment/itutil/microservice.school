﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microservice.School.Database
{
    public class Grade
    {
        /// <summary>
        /// Gets or sets Id.
        /// </summary>
        [BsonId] // converts the Property to _id. Auto generates Guid.
        public Guid Id { get; set; }
        /// <summary>
        /// Gets or sets Year.
        /// </summary>
        public GradeYear Year { get; set; }
        /// <summary>
        /// Gets or sets Name.
        /// </summary>
        public string Name
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets FullName.
        /// </summary>
        /// 
        public string FullName
        {
            get
            {
                var fullName = "";
                switch (Year)
                {
                    case GradeYear.Zero:
                    case GradeYear.One:
                    case GradeYear.Two:
                    case GradeYear.Three:
                    case GradeYear.Four:
                    case GradeYear.Five:
                    case GradeYear.Six:
                    case GradeYear.Seven:
                    case GradeYear.Eight:
                    case GradeYear.Nine:
                    case GradeYear.Ten:
                        if (Name.Length > 0)
                        {
                            fullName = ((int)Year - 100) + "." + Name;
                        }
                        else
                        {
                            fullName = ((int)Year - 100).ToString();
                        }
                        break;
                    case GradeYear.Teacher:
                    case GradeYear.Special:
                        fullName = Name;
                        break;
                    default:
                        break;
                }
                return fullName;
            }
        }
        /// <summary>
        /// Gets or sets IsDisabled.
        /// </summary>
        /// 
        public bool IsDisabled { get; set; }
    }
}
