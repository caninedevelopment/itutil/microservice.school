﻿namespace Microservice.School
{
    using System;
    using System.Collections.Generic;
    using MongoDB.Bson;
    using MongoDB.Bson.Serialization.Conventions;
    using MongoDB.Driver;

    /// <summary>
    /// Datacontext for the schoolDatabase.
    /// </summary>
    public class DataContext
    {
        private readonly string dbName = "schoolDatabase";
        private readonly string collectionName = "school";
        private readonly IMongoDatabase mongoDb;
        /// <summary>
        /// Initializes a new instance of the <see cref="DataContext"/> class.
        /// </summary>
        public DataContext()
        {
            var customConventions = new ConventionPack {
                new IgnoreExtraElementsConvention(true), // Ignore the extra fields that a document has when compared to the provided DTO on Deserialization.  
                new IgnoreIfDefaultConvention(true) // Ignore the fields with default value on Serialization. Keeps the document smaller.
            };
            ConventionRegistry.Register("CustomConventions", customConventions, type => true); // how to handle different encounters during serialization & deserialization.
            var settings = ITUtil.Common.Config.GlobalRessources.getConfig();
            if (settings == null)
            {
                throw new Exception("GlobalRessource settings were not provided");
            }
            var NOSQL = settings.GetSetting<ITUtil.Common.Config.NoSQLSettings>();
            var client = new MongoClient(NOSQL.ConnectionString());
            mongoDb = client.GetDatabase(NOSQL.GetFullDBName(dbName)); // automatically creates a database if it doesn't exists
        }
        public void InsertSchool(Database.School document) 
        {
            var productCollection = this.mongoDb.GetCollection<Database.School>(collectionName);
            productCollection.InsertOne(document);
        }

        public List<Database.School> GetSchools()
        {
            var schoolCollection = mongoDb.GetCollection<Database.School>(collectionName);
            return schoolCollection.Find(school => true).ToList();
        }
        public Database.School GetSchoolById(Guid id)
        {
            var schoolCollection = mongoDb.GetCollection<Database.School>(collectionName);
            var filter = Builders<Database.School>.Filter.Eq("Id", id);
            return schoolCollection.Find(filter).FirstOrDefault();
        }

        public void UpdateSchool(Guid id, Database.School school)
        {
            var schoolCollection = mongoDb.GetCollection<Database.School>(collectionName);
            var filter = Builders<Database.School>.Filter.Eq("Id", id);
            schoolCollection.ReplaceOne(
                filter,
                school
            );
        }

        public void DeleteSchool(Guid id)
        {
            var schoolCollection = mongoDb.GetCollection<Database.School>(collectionName);
            var filter = Builders<Database.School>.Filter.Eq("Id", id);
            schoolCollection.DeleteOne(filter);
        }

    }
}
